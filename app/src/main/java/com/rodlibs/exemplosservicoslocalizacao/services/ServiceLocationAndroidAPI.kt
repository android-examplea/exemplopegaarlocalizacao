package com.rodlibs.exemplosservicoslocalizacao.services

import android.Manifest
import android.annotation.SuppressLint
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.GpsStatus
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.os.IBinder
import android.os.Looper
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.rodlibs.exemplosservicoslocalizacao.utils.NotificationControl

class ServiceLocationAndroidAPI : Service(), LocationListener, GpsStatus.Listener {


    companion object{val BROADCAST_LOCATION_ANDROID_API: String = "br.com.location.androidORGoogle.api"}
    private val TAG: String = "LOCATION_ANDROID"
    private var locationManager: LocationManager? = null
    private val DISTANCE: Float = 0F
    private val TIME: Long = 5000



    override fun onCreate() {
        super.onCreate()
        Log.i(TAG, "onCreate")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notification = NotificationControl.getNotification(this)
            startForeground(NotificationControl.FOREGROUND_SERVICE_NOTIFICATION_ID, notification)
        }
    }


    override fun onBind(intent: Intent): IBinder? { return null }


    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.i(TAG, "onStartCommand")
        initInstanceLocationANDCallback()
        return START_STICKY
    }


    override fun onDestroy() {
        super.onDestroy()
        Log.i(TAG, "onDestroy")
        if (locationManager != null) {
            locationManager?.removeUpdates(this)
        }
    }



    private fun initInstanceLocationANDCallback(){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return
        }

        // LocationManage da API nativa do android
        if (locationManager == null) {
            locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
            locationManager?.requestLocationUpdates(LocationManager.GPS_PROVIDER, TIME, DISTANCE, this)
            locationManager?.addGpsStatusListener(this)
        }
    }




    // método que recebe uma location da API LocationManage nativo do android
    @SuppressLint("MissingPermission")
    override fun onLocationChanged(location: Location) {
        sendDataBroadcast(location)
        var satellitesInFix = 0
        for (sat in locationManager?.getGpsStatus(null)?.satellites!!) {
            if (sat.usedInFix()) {
                satellitesInFix++
            }
        }
        Log.i(TAG, "LAT: ${location.latitude} - LNG: ${location.longitude}  -  $satellitesInFix Satelites")
    }

    // método que indica se o gps foi desativado da API LocationManage nativo do android
    override fun onProviderDisabled(provider: String) {
        Log.i(TAG, "desativou: $provider")
        sendDataBroadcast(null, "Desativou a localização")
    }
    // método que indica se o gps foi ativado da API LocationManage nativo do android
    override fun onProviderEnabled(provider: String) {
        Log.i(TAG, "ativou: $provider")
        sendDataBroadcast(null, "Ativou a localização")
    }




    // Obtém a quantidade de satélites que foi utilizada
    @SuppressLint("MissingPermission")
    override fun onGpsStatusChanged(i: Int) {
        var satellitesInFix = 0
        for (sat in locationManager?.getGpsStatus(null)?.satellites!!) {
            if (sat.usedInFix()) {
                satellitesInFix++
            }
        }
        Log.i(TAG, "$satellitesInFix Satelites")
    }



    // BroadCast de comunicação
    private fun sendDataBroadcast(location: Location?, status: String = ""){
        val intent = Intent(BROADCAST_LOCATION_ANDROID_API)
        intent.putExtra("locationBroad", location)
        intent.putExtra("status", status)
        LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(intent)
    }
}