package com.rodlibs.exemplosservicoslocalizacao.services

import android.Manifest
import android.annotation.SuppressLint
import android.app.Service
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.Build
import android.os.IBinder
import android.os.Looper
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.android.gms.location.*
import com.rodlibs.exemplosservicoslocalizacao.utils.NotificationControl


/* Documentação: https://developer.android.com/training/location/change-location-settings */


class ServiceLocationGoogleAPI : Service(){


    private val TAG: String = "LOCATION_GOOGLE"
    private val BROADCAST_LOCATION_GOOGLE_API: String = "br.com.location.androidORGoogle.api"
    private var fusedLocationClient: FusedLocationProviderClient? = null
    private var locationCallback: LocationCallback? = null
    private var locationRequest: LocationRequest? = null
    private var locationManager: LocationManager? = null
    private val PASSIVE_INTERVAL: Long = 10 * 1000
    private val PASSIVE_FAST_INTERVAL: Long = 5000

    private val LOTE_INTERVAL: Long = 2 * 60 * 1000
    private val LOTE_MAX_WAIT_TIME: Long = 10 * 60 * 1000
    private var isActiveFusedLocation = false





// -------------------------------------------- MÉTODOS NATIVOS DO SERVICE ----------------------------------------------------------
    override fun onCreate() {
        super.onCreate()
        Log.i(TAG, "onCreate")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notification = NotificationControl.getNotification(this)
            startForeground(NotificationControl.FOREGROUND_SERVICE_NOTIFICATION_ID, notification)
        }
    }


    @SuppressLint("MissingPermission")
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.i(TAG, "onStartCommand")
        initInstanceFusedANDCallback()
        return START_STICKY
    }


    override fun onBind(intent: Intent): IBinder? {
        return null
    }


    override fun onDestroy() {
        super.onDestroy()
        Log.i(TAG, "onDestroy")
        if (fusedLocationClient != null) {
            fusedLocationClient?.removeLocationUpdates(locationCallback)
        }
    }



// -------------------------------------------- METODO QUE INICIALIZA O LOCATION DO GOOGLE ----------------------------------------------------------
    private fun initInstanceFusedANDCallback(){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return
        }

        if (fusedLocationClient == null){
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        }

        if (locationCallback == null){
            locationCallback = object : LocationCallback() {
                @SuppressLint("MissingPermission")
                override fun onLocationResult(locationResult: LocationResult?) {
                    locationResult ?: return
                    for (location in locationResult.locations){
                        Log.i(TAG, "LAT: ${location.latitude} - LNG: ${location.longitude}")

                        //Broadcast para comunicação, que passa o location
                        sendDataBroadcast(location)
                    }
                }

                override fun onLocationAvailability(locationAvailabi: LocationAvailability) {
                    if (locationAvailabi.isLocationAvailable){
                        sendDataBroadcast(null, "Ativou a localização")
                    }else{
                        sendDataBroadcast(null, "Desativou a localização")
                    }
                }
            }
        }

        if (locationRequest == null) {
            //Para solicitação passivas
            locationRequest = LocationRequest.create().apply {
                interval = PASSIVE_INTERVAL
                fastestInterval = PASSIVE_FAST_INTERVAL
                priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            }
            //Para solicitação em lotes
//            locationRequestLote = LocationRequest.create().apply {
//                interval = LOTE_INTERVAL
//                maxWaitTime = LOTE_MAX_WAIT_TIME
//                priority = LocationRequest.PRIORITY_HIGH_ACCURACY
//            }
        }
        // Para criar varios request
        // val builder = LocationSettingsRequest.Builder()
        // builder.addLocationRequest(mLocationRequest).build()


        if (!isActiveFusedLocation) {
            fusedLocationClient?.requestLocationUpdates(locationRequest!!, locationCallback!!, Looper.getMainLooper())
            isActiveFusedLocation = true
        }








    }


    private fun sendDataBroadcast(location: Location?, status: String = ""){
        val intent = Intent(BROADCAST_LOCATION_GOOGLE_API)
        intent.putExtra("locationBroad", location)
        intent.putExtra("status", status)
        LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(intent)
    }
}