package com.rodlibs.exemplosservicoslocalizacao

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.rodlibs.exemplosservicoslocalizacao.services.ServiceLocationAndroidAPI
import com.rodlibs.exemplosservicoslocalizacao.services.ServiceLocationAndroidAPI.Companion.BROADCAST_LOCATION_ANDROID_API
import com.rodlibs.exemplosservicoslocalizacao.services.ServiceLocationGoogleAPI
import com.rodlibs.exemplosservicoslocalizacao.utils.*


class MainActivity : AppCompatActivity() {


    var mMessageReceiver: BroadcastReceiver? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        initBroadcast()

        findViewById<Button>(R.id.btServiceGoogle).setOnClickListener {
            if (checkGPSActive()) {
                if (!checkServiceIsRunningmy(ServiceLocationGoogleAPI::class.java)) {
                    openService(ServiceLocationGoogleAPI::class.java)
                }
            }else{
                setDialog("GPS desativado, por favor ative seu gps.")
            }
        }


        findViewById<Button>(R.id.btServiceAndroid).setOnClickListener {
            if (checkGPSActive()) {
                if (!checkServiceIsRunningmy(ServiceLocationAndroidAPI::class.java)) {
                    openService(ServiceLocationAndroidAPI::class.java)
                }
            }else{
                setDialog("GPS desativado, por favor ative seu gps.")
            }
        }


        findViewById<Button>(R.id.btGetLocation).setOnClickListener {
            if (checkGPSActive()){

                // Obter 1 localização pela API de Location do Android
//                val location = GetLocationActual(applicationContext).getLocation()
//                findViewById<TextView>(R.id.txtLocation).text = "LAT: ${location?.latitude} - LNG: ${location?.longitude}"


                // Obter 1 localização pela API de Location do Google
                GetLocationActualGoogle(applicationContext).getLocation(object : GetLocationActualGoogle.CallBackLocationAPIGoogle{
                    override fun getLocationGoogleApi(location: Location?) {
                        findViewById<TextView>(R.id.txtLocation).text = "LAT: ${location?.latitude} - LNG: ${location?.longitude}"
                    }
                })

            }else{
                setDialog("GPS desativado, por favor ative seu gps.")
            }
        }
    }



    private fun initBroadcast(){
        mMessageReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent) {

                val location = intent.getParcelableExtra<Location>("locationBroad")
                if (location != null){
                    findViewById<TextView>(R.id.txtLatLngServices).text = "LAT: ${location?.latitude} - LNG: ${location?.longitude}"
                }else{
                    val status = intent.getStringExtra("status")
                    findViewById<TextView>(R.id.txtLatLngServices).text = status
                }
            }
        }
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver!!, IntentFilter(BROADCAST_LOCATION_ANDROID_API));
    }



    override fun onDestroy() {
        super.onDestroy()
        if (mMessageReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver!!)
        }
    }



    private fun setDialog(msg: String){
        val alert: AlertDialog.Builder = AlertDialog.Builder(this)
        alert.setMessage(msg)
        alert.setPositiveButton("SIM") { dialog, which ->
            startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
            dialog.dismiss()
        }
        alert.setNegativeButton("NÃO") { dialog, which ->
            dialog.dismiss()
        }
        alert.show()
    }
}