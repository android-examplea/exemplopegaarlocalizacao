package com.rodlibs.exemplosservicoslocalizacao.utils

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import androidx.core.app.ActivityCompat
import java.io.IOException
import java.lang.Exception


class GetLocationActual(val context: Context){


    private var locationManager: LocationManager? = null
    private var myLocation: Location? = null


    @Throws(Exception::class)
    fun getLocation(): Location? {
        locationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return null
        }

        if (!locationManager?.isProviderEnabled(LocationManager.GPS_PROVIDER)!!) {
            throw Exception("GPS desativado, por favor ative seu gps.")
        }
        myLocation = getLastKnownLocation()
        return myLocation
    }



    private fun getLastKnownLocation(): Location? {
        val providers: List<String>? = locationManager?.getProviders(true)
        var bestLocation: Location? = null

        if (providers != null) {
            for (provider in providers) {
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    break
                }
                val l: Location = locationManager?.getLastKnownLocation(provider) ?: continue
                if (bestLocation == null || l.accuracy < bestLocation.accuracy) {
                    // Found best last known location: %s", l);
                    bestLocation = l
                }
            }
        }
        return bestLocation
    }



}