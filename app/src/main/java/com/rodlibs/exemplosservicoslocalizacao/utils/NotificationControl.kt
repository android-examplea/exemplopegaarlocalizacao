package com.rodlibs.exemplosservicoslocalizacao.utils

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.graphics.Color
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.rodlibs.exemplosservicoslocalizacao.R

class NotificationControl {


    companion object {

        val FOREGROUND_SERVICE_NOTIFICATION_ID = 2
        private val NOTIFICATION_CHANNEL_ID = "com.rodlibs.exemplosservicoslocalizacao"
        private val CHANNEL_NAME = "Serviço em segundo plano"

        @RequiresApi(api = Build.VERSION_CODES.O)
        fun getNotification(context: Context): Notification? {
            val chan = NotificationChannel(
                NOTIFICATION_CHANNEL_ID,
                CHANNEL_NAME,
                NotificationManager.IMPORTANCE_NONE
            )
            chan.lightColor = Color.BLUE
            chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
            val manager =
                (context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager)
            manager.createNotificationChannel(chan)
            val notificationBuilder = NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID)
            return notificationBuilder.setOngoing(true)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("O app está rodando em segundo plano")
                .setPriority(NotificationManager.IMPORTANCE_MIN)
                .setCategory(Notification.CATEGORY_SERVICE)
                .build()
        }
    }
}