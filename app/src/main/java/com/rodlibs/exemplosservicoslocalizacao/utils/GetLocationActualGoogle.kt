package com.rodlibs.exemplosservicoslocalizacao.utils
import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.os.Looper
import android.util.Log
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.*

class GetLocationActualGoogle(val context: Context, val listener: CallBackLocationAPIGoogle) {


    private var fusedLocationClient: FusedLocationProviderClient? = null
    private var locationCallback: LocationCallback? = null
    private var locationRequest: LocationRequest? = null
    private val INTERVAL: Long = 2 * 1000
    private val FAST_INTERVAL: Long = 2 * 1000


    fun getLocation() {
        if (ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        initInstanceFusedANDCallback()
    }


    @SuppressLint("MissingPermission")
    private fun initInstanceFusedANDCallback() {
        if (fusedLocationClient == null) {
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)
        }

        if (locationCallback == null) {
            locationCallback = object : LocationCallback() {
                @SuppressLint("MissingPermission")
                override fun onLocationResult(locationResult: LocationResult?) {
                    locationResult ?: return
                    for (location in locationResult.locations) {
                        if (location != null && location.hasAccuracy() && location.accuracy <= 50) {
                            listener.getLocationGoogleApi(location)
                            destroyListeners()
                            return
                        }
                    }
                }
            }
        }

        if (locationRequest == null) {
            locationRequest = LocationRequest.create().apply {
                interval = INTERVAL
                fastestInterval = FAST_INTERVAL
                priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            }
        }

        fusedLocationClient?.requestLocationUpdates(
            locationRequest!!,
            locationCallback!!,
            Looper.getMainLooper()
        )
    }


    fun destroyListeners() {
        Log.i("ONE_LOCA", "onDestroy")
        if (fusedLocationClient != null && locationCallback != null) {
            fusedLocationClient?.removeLocationUpdates(locationCallback)
        }
        if (locationCallback != null) {
            locationCallback = null
        }
        if (locationRequest != null) {
            locationRequest = null
        }
        fusedLocationClient = null
    }


    interface CallBackLocationAPIGoogle {
        fun getLocationGoogleApi(location: Location)
    }
}