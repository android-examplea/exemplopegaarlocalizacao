package com.rodlibs.exemplosservicoslocalizacao.utils

import android.app.Activity
import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.location.LocationManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import java.lang.Exception


fun Activity.checkServiceIsRunningmy(myClass: Class<*>): Boolean {
    var isServiceRunning = false
    val manager = this.getSystemService(AppCompatActivity.ACTIVITY_SERVICE) as ActivityManager
    for (service in manager.getRunningServices(Int.MAX_VALUE)) {
        if (myClass.name == service.service.className) {
            isServiceRunning = true
            break
        }
    }
    return isServiceRunning
}


fun Activity.openService(myService: Class<*>) {
    val intent = Intent(this, myService)
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        startForegroundService(intent)
    } else {
        startService(intent)
    }
}


fun Activity.checkGPSActive(): Boolean{
    val locationManager = this.getSystemService(Context.LOCATION_SERVICE) as LocationManager
    return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
}