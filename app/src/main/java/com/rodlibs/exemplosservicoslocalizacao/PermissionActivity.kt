package com.rodlibs.exemplosservicoslocalizacao

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat


class PermissionActivity : AppCompatActivity()  {


    private val PERMISSION_BACKGROUND_LOCATION = 1
    private val PERMISSION_FORENGROUND_LOCATION = 2
    private var dialogPermissionAndroid11: android.app.AlertDialog? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_permission)

        checkPermission()
    }



    private fun checkPermission(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
            checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
            checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION), PERMISSION_FORENGROUND_LOCATION
            )
        }
        else if (Build.VERSION.SDK_INT > 28 &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_BACKGROUND_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                dialogInfoPermission(false)
        }
        else{
            openNextView()
        }
    }



    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == PERMISSION_FORENGROUND_LOCATION) {
            if (grantResults.size == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                checkPermission()
            } else {
                setDialog("Para Continuar você precisa fornecer a permissão, para utiliza-mos seu GPS.")
            }
        }
        else if (requestCode == PERMISSION_BACKGROUND_LOCATION) {
            openNextView()
        }
    }




    private fun dialogInfoPermission(isRecused: Boolean) {
        val view: View = layoutInflater.inflate(R.layout.dialog_infor_permission, null)
        view.findViewById<View>(R.id.buttonRecuse).setOnClickListener {
            dialogPermissionAndroid11?.dismiss()
            openNextView()
        }
        view.findViewById<View>(R.id.buttonEnable).setOnClickListener {
            if (isRecused) {
                callViewDetailAppForPermissions()
            } else {
                // Android 10, chama o dialog do requestPermission
                if (Build.VERSION.SDK_INT == 29) {
                    requestPermissions(arrayOf(Manifest.permission.ACCESS_BACKGROUND_LOCATION), PERMISSION_BACKGROUND_LOCATION)
                } else if (Build.VERSION.SDK_INT == 30) {
                    callViewDetailAppForPermissions()
                }
            }
            dialogPermissionAndroid11?.dismiss()
        }
        val builder = android.app.AlertDialog.Builder(this)
        builder.setView(view)
        dialogPermissionAndroid11 = builder.create()
        dialogPermissionAndroid11?.setCancelable(false)
        if (!dialogPermissionAndroid11?.isShowing!!) {
            dialogPermissionAndroid11?.show()
        }
    }

    private fun callViewDetailAppForPermissions() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package", packageName, null)
        intent.data = uri
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
    }



    private fun openNextView(){
        startActivity(Intent(this, MainActivity::class.java))
    }



    private fun setDialog(msg: String){
        val alert: AlertDialog.Builder = AlertDialog.Builder(this)
        alert.setMessage(msg)
        alert.setPositiveButton("OK") { dialog, which ->
            dialog.dismiss()
        }
        alert.show()
    }
}